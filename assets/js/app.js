$(document).ready(function () {

  $(window).scroll(function() {
    var elem = $('.c-navbar'),
      top = $(this).scrollTop();
    if (top > 100) {
      elem.removeClass('unscrolled');
    } else {
      elem.addClass('unscrolled');
    }
  });

  var sliders = [$('#parters-slider'), $('#investors-slider')];

  sliders.forEach(function(slider, id) {
    $($(".owl-n-next")[id]).on('click', function(e){
      e.preventDefault();
      slider.trigger('owl.next');
    });

    $($(".owl-n-prev")[id]).on('click', function(e){
      e.preventDefault();
      slider.trigger('owl.prev');
    });

    slider.owlCarousel({
      items: 6,
      pagination: false,
      rewindSpeed: 100 * (id+1)/2 + 1000/(id+1),
      autoPlay: true,
      itemsMobile: [767, 1]
    });
  });

  $('.scroll-to').on('click', function () {
    event.preventDefault();
    var id = $(this).attr('href'),
        top = $(id).offset().top - 75;
    $('body,html').animate({scrollTop: top}, 500);
  });

  $('.js-btn-play').on('click', function() {
    $(this).hide().closest('.video-wrapper').addClass('without-bg');
    var video = $('#n-video');
    video.attr('controls', true);
    video.addClass('show');
    video[0].play();
  });

});
